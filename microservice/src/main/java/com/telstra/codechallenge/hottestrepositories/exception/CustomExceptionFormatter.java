package com.telstra.codechallenge.hottestrepositories.exception;

import lombok.AllArgsConstructor;
import lombok.Value;

/**
 * 
 * @author nagendra.kola
 *
 *         This class used for the formatt of error details.
 */

@AllArgsConstructor
@Value
public class CustomExceptionFormatter {
	private String errorMessage;
	private String errorDescription;

}
