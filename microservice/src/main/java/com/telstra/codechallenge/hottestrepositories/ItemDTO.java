package com.telstra.codechallenge.hottestrepositories;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author nagendra.kola
 * 
 *         This class for holding the consumed data
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemDTO {

	@JsonProperty("html_url")
	private String htmlUrl;
	@JsonProperty("watchers_count")
	private Long watchersCount;
	@JsonProperty("language")
	private String language;
	@JsonProperty("description")
	private String description;
	@JsonProperty("name")
	private String name;

}
