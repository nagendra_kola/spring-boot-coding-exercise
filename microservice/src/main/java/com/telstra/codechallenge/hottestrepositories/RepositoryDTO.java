package com.telstra.codechallenge.hottestrepositories;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author nagendra.kola
 * 
 *         This is DTO class used to hold list of items.
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryDTO {

	@JsonProperty("items")
	private List<ItemDTO> items;

}
