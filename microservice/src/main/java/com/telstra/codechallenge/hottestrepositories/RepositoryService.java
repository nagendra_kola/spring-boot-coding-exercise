package com.telstra.codechallenge.hottestrepositories;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.telstra.codechallenge.hottestrepositories.exception.GitHubRepositoryException;
import com.telstra.codechallenge.hottestrepositories.util.PathVariablesConstants;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author nagendra.kola
 * 
 *         This is service class where we are consuming the rest api and return
 *         the result.
 *
 */
@Service
@Slf4j
public class RepositoryService {

	@Autowired
	RestTemplate restTemplate;

	@Value("${github.reposioty.base.url}")
	private String rootURI;

	@Value("${github.repository.sort.value}")
	private String sortValue;

	@Value("${github.repository.order.value}")
	private String orderValue;

	@Value("${github.repository.default.pageSize}")
	private int defaultValue;

	// This method is used for return fullURI
	private String getFullURI(String rootURI, String weekStart, String weekEnd, int page, int perPage) {

		String urlString = UriComponentsBuilder.fromUriString(rootURI)
				.queryParam(PathVariablesConstants.PATH_START, PathVariablesConstants.WEEK_FIRST_MORE + weekStart)
				.queryParam(PathVariablesConstants.WEEK_LAST_LESS, weekEnd)
				.queryParam(PathVariablesConstants.SORT, sortValue).queryParam(PathVariablesConstants.ORDER, orderValue)
				.queryParam(PathVariablesConstants.PAGE, page).queryParam(PathVariablesConstants.PER_PAGE, perPage)
				.build().toUriString();
		return urlString;

	}

	public ResponseEntity<List<ItemDTO>> getAllRepositories(Integer size) {
		try {
			LocalDate now = LocalDate.now();
			String weekStart = now.minusDays(7 + now.getDayOfWeek().getValue() - 1).toString();
			String weekEnd = now.minusDays(now.getDayOfWeek().getValue()).toString();
			log.info("Fetching repositories details between weeks {} and {}", weekStart, weekEnd);
			// Implementing Pagenation using perpage and pagesize
			List<ItemDTO> entities = new ArrayList<>();
			int pageCount = (size / defaultValue) + 1;
			int finalperPageSize = size % defaultValue;
			int pageSize = defaultValue;
			if (finalperPageSize == 0) {
				pageCount -= 1;
				finalperPageSize = pageSize;
			}
			for (int i = 1; i <= pageCount; i++) {
				if (i == pageCount) {
					pageSize = finalperPageSize;
				}
				String fullURI = getFullURI(rootURI, weekStart, weekEnd, i, pageSize);
				log.info("Full URI {}", fullURI);
				// Consuming the rest api
				ResponseEntity<RepositoryDTO> entity = restTemplate.getForEntity(fullURI, RepositoryDTO.class);
				entities.addAll(entity.getBody().getItems());
			}
			return new ResponseEntity<List<ItemDTO>>(entities, HttpStatus.OK);
		} catch (Exception ex) {
			throw new GitHubRepositoryException(ex.getMessage(), PathVariablesConstants.CUSTOM_EXCEPTION);
		}
	}
}
