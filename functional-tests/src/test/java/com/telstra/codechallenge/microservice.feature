# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to know if my spring boot application is running

  Scenario: Is the info uri available and returning data for two records
    Given url microserviceUrl
    And path '/search'
    And  param size = 2
    When method GET
    Then status 200
    And match  response == [{html_url: '#string',watchers_count: '#number',language: '#string',description: '#string',name: '#string'},{html_url: '#string',watchers_count: '#number',language: '#string',description: '#string',name: '#string'}]
    And match each response contains {html_url: '#string',watchers_count: '#number',language: '#',description: '#',name: '#string'}
    * def len = response.length
    * match len == 2
    
  Scenario: Is the info uri available and returning data for fifty records
    Given url microserviceUrl
    And path '/search'
    And  param size = 50
    When method GET
    Then status 200
    And match each response contains {html_url: '#string',watchers_count: '#number',language: '#',description: '#',name: '#string'}
    * def len = response.length
    * match len == 50
